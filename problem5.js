const allTheYear = function (inventory) {
    let allOldYear = [];
    let count = 0;
    for(let i=0; i<inventory.length;i++) {
        if (inventory[i].car_year < 2000) {
            allOldYear.push(inventory[i].car_year);
            count++;
        }
    }
    return {allOldYear, count}
}

module.exports = {
    allTheYear : allTheYear
}