const sortByAlpha = function(inventory) {
    let copyInventory = inventory;
    let len = copyInventory.length;

    for(let index1 = 0; index1 < len; index1++ ) {

        for(let index2 = index1+1; index2 < len; index2++) {
            let a = copyInventory[index1].car_model.toLowerCase();
            let b = copyInventory[index2].car_model.toLowerCase();
            
            let temp ;
            if(a > b) {
                
                temp = copyInventory[index1];
                copyInventory[index1] = copyInventory[index2];
                copyInventory[index2] = temp;

            }
        }
    }
    
    return copyInventory;
}

module.exports = {
    sortByAlpha : sortByAlpha
}