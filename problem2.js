const lastCarInfo = function (inventory) {
    let lastIndex = inventory.length;
    return "Last car is a"+" "+inventory[lastIndex-1].car_make+" "+inventory[lastIndex-1].car_model;
}

module.exports = {
    lastCarInfo : lastCarInfo
}