const problem5 = require('../problem5');
const data = require('../inventory');

const result = problem5.allTheYear(data.inventory);
// old year (before 2000)
/* expected old years
allOldYear: [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
  ],
*/
let expOutArray = [
    1983, 1990, 1995, 1987, 1996,
    1997, 1999, 1987, 1995, 1994,
    1985, 1997, 1992, 1993, 1964,
    1999, 1991, 1997, 1992, 1998,
    1965, 1996, 1995, 1996, 1999
  ];
let expCount = 25;
let flag = true;

for(let index = 0; index < expOutArray.length; index++) {
    // matching expectedOutput with actual output if any value is not matched flag set to be false
    if(expOutArray[index] !== result.allOldYear[index]) {
            flag = false;
    }
}

if(flag && result.count === expCount) {
    console.log(result);
}