const problem2 = require('../problem2');
const data = require('../inventory');

const result = problem2.lastCarInfo(data.inventory);
// testing for output
// expected output "Last car is a Lincoln Town Car"

let expOutput = "Last car is a Lincoln Town Car";
if(expOutput === result) {
    console.log(result);
}