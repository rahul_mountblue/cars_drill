const allTheYear = function (inventory) {
    let allYear = [];
    for(let i=0; i<inventory.length;i++) {
        allYear.push(inventory[i].car_year);
    }
    return allYear;
}

module.exports = {
    allTheYear : allTheYear
}